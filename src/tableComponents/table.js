import React, { Component } from 'react';

const table = ( props ) => {
    return (
        <tr>
            <td>{props.movie.name}</td>
            <td>{props.movie.year}</td>
            <td>{props.movie.rating}</td>
            <td>{props.movie.comment}</td>
            <td onClick={props.click}><button type="button" class="btn btn-danger">Delete</button></td>
        </tr>)
}

export default table;