import React from 'react';

const movieForm = ( props ) => {
    return (
        <div class="col-lg-6">
        <form>
            <div class="input-group mb-3">
            <input id="name" placeholder="Name" onChange={props.nameChange} class="form-control" required></input>
            <input id="year" placeholder="Year" onChange={props.nameChange} class="form-control" required></input>
            <input type="" id="rating" placeholder="Rating" onChange={props.nameChange} class="form-control" required></input>
            <input id="comment" placeholder="Comment" onChange={props.nameChange} class="form-control" required></input>
                    <button onClick={props.click} class="btn btn-primary">Submit</button>
            </div>
        </form>
        </div>
    )
}
export default movieForm;