import React, { Component } from 'react';
import Table from './tableComponents/table';
import MovieForm from './tableComponents/MovieForm';
const URL = 'http://localhost:3001/movies/';

class MovieManager extends Component {
  state = {
  }

  constructor() {
    super();
  }

  componentDidMount() {
    fetch(URL, {
        method: 'GET',
        headers: new Headers()
      })
      .then(response => response.json())
      .then(data => {
        this.setState({movies: data});
      });
  }
  deleteTableRow = (movieID) => {
    fetch(URL + movieID, {
      method: 'DELETE',
    })
    .then(
      fetch(URL, {
        method: 'GET',
        headers: new Headers()
      }).then(response => response.json())
        .then(data => {
          this.setState({ movies: data });
        })
    )
  }

  nameChangeHandler = (event) => {
    this.setState({[event.target.id] : event.target.value});
  }
  addMovieValueChecker = () => {
    if (!parseInt(this.state.rating) || parseInt(this.state.rating) > 10) {
      return true;
    }
    if (!parseInt(this.state.year)) {
      return true;
    }
    return false;
  }
  addMovie = () => {
    //Checks if the input is valid
    if (this.addMovieValueChecker()) {
      alert('Invalid input!');
      return;
    }
    fetch(
      URL +
      '?name=' +
      this.state.name +
      '&year=' +
      this.state.year +
      '&rating=' +
      this.state.rating +
      '&comment=' +
      this.state.comment, {
      method: 'POST'
    })
      .then(
        fetch(URL, {
          method: 'GET',
          headers: new Headers()
        }).then(response => response.json())
          .then(data => {
            this.setState({ movies: data });
          })
      )
  }
  render() {
    let movies = null;

    if(this.state.movies) {
      movies = (
        <div>
          <table class="table">
            <thead class="thead-dark">
              <tr>
                <th>Name</th>
                <th>Year</th>
                <th>Rating</th>
                <th>Comment</th>
                <th>Remove</th>
              </tr>
            </thead>
            <tbody>
              {this.state.movies.map((movie, index) => {
                return <Table
                  click={() => this.deleteTableRow(movie.id)}
                  key={movie.id}
                  movie={movie}
                />
              })}
            </tbody>
          </table>
        </div>
        )
    }

    return (
      <div>
        {movies}
        <MovieForm
          nameChange={(event) => this.nameChangeHandler(event)}
          click={() => this.addMovie()}
        />
      </div>
    )
  }

}

export default MovieManager;
